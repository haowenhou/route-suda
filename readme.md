## 简介

在校外通过VPN访问相关网站或者计算服务器时，因为VPN接管了全局流量导致上网速度可能很慢。这个脚本通过修改Windows路由表的方式仅允许特定流量通过VPN，从而使其他网站访问速度恢复正常。

## 使用方法

先登录VPN客户端，之后运行 ~~bat~~ Python 脚本即可 ~~（bat脚本用来以管理员权限运行旁边的Python脚本）~~。可以使用命令行参数 `--name` 指定具体配置文件（默认sudo），如：

```powershell
python main.py --name suda
```

请确保 Python 脚本所在目录下存在 config 配置文件夹。

需要通过VPN的域名和IP在 ~~Python程序中~~ [config](config) 目录下对应的配置文件中指定，默认为 suda 。添加了ACM、IEEE Library、苏大的通知发文等。如需连接服务器，可在IPS中添加服务器IP。

应该可以适用很多VPN，用route print命令观察路由ip前缀并修改程序中的GATEWAY_PREFIX即可，比如苏大是10.60，交大是10.184。


## Changes 

### 230907 by [holger](https://github.com/HolgerZhang)

- 修改了程序的文件名
- 将route命令替换为 `C:\\Windows\\System32\\route.exe` 以解决潜在的 "无法初始化设备 PRN" 问题
- 将GATEWAY_PREFIX、SITES、IPS单独抽离为JSON配置文件，存放在 [config](config) 目录下，以支持更多学校的VPN；同时 [main.py](main.py) 提供一个命令行参数 `--name` 指定具体配置文件
- 添加了南大的相关配置，欢迎更多学校来补充和完善
- 添加了自动获取管理员权限和自动切换工作目录至脚本所在目录的功能，现已可以通过直接运行Python脚本来启动（bat脚本可以下岗了）
- 对IP列表去重，并忽略IPv6
- 添加了一些文本输出，并在可能失败的情况下提示用户
