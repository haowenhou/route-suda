import argparse
import ctypes
import json
import os
import re
import socket
import sys


def resolve_domain_ips(domain):
    """Resolve IPs for a domain."""
    try:
        return [ip[4][0] for ip in socket.getaddrinfo(domain, None)]
    except Exception as e:
        print(f"Error resolving IP for {domain}: {e}")
        return []


def main(config):
    """ main process """
    SITES = config['sites']
    IPS = config['ip_list']
    GATEWAY_PREFIX = config['gateway_prefix']

    for site in SITES:
        IPS.extend(resolve_domain_ips(site))
    IPS = set(filter(lambda ip_item: ':' not in ip_item, IPS))  # Remove duplicate IP and remove IPv6

    print('> C:\\Windows\\System32\\route.exe print')
    route_table = os.popen('C:\\Windows\\System32\\route.exe print')
    route_table = route_table.readlines()

    gateways = set()
    for line in route_table:
        entry = re.split('[ ]+', line.strip())
        if len(entry) == 5 and entry[2].startswith(GATEWAY_PREFIX):
            print(f'> C:\\Windows\\System32\\route.exe delete {entry[0]} {entry[2]}')
            os.system(f'C:\\Windows\\System32\\route.exe delete {entry[0]} {entry[2]}')
            gateways.add(entry[2])

    print(f"Gateways: {gateways}")
    print(f"IPs: {IPS}")
    # Add routes for IPs.
    if len(gateways) == 0:
        print("Maybe you're not using VPN or something wrong in `gateway_prefix`! `route print` goes here:")
        print(''.join(route_table))
        input('Enter to exit...')
        exit()
    for gateway in gateways:
        for ip in IPS:
            print(f'> C:\\Windows\\System32\\route.exe add {ip} mask 255.255.255.255 {gateway}')
            os.system(f'C:\\Windows\\System32\\route.exe add {ip} mask 255.255.255.255 {gateway}')
    print('Finished!')


def is_admin():
    """ check admin rights """
    try:
        return ctypes.windll.shell32.IsUserAnAdmin()
    except:
        return 0


if __name__ == '__main__':
    if is_admin():
        os.chdir(os.path.dirname(__file__))  # Change work dir
        parser = argparse.ArgumentParser(description='Route for VPN')
        parser.add_argument('--name', type=str, help='configuration file name, e.g. "suda".',
                            required=False, default='suda')
        args = parser.parse_args()
        config_name = f'config/{args.name}.json'
        print(f'Script args:', sys.argv[1:])
        print(f'Using config: {config_name}')
        with open(config_name, 'r', encoding='utf-8') as config_file:
            config = json.load(config_file)
        main(config)
    else:
        # Re-run the program with admin rights
        print("Re-run with admin rights, args:", sys.argv[1:])
        re_run_file = __file__ + ' ' + ' '.join(sys.argv[1:]) if len(sys.argv[1:]) > 0 else __file__
        ctypes.windll.shell32.ShellExecuteW(None, "runas", sys.executable, re_run_file, None, 1)
